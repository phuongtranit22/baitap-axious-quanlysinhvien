const BASE_URL = "https://62db6cb3e56f6d82a77285da.mockapi.io";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
getDSSV();

function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function themSV() {
  // newSv = object lấyb từ form
  let newSV = {
    id: document.getElementById('txtMaSV').value,
    name: document.getElementById('txtTenSV').value,
    email: document.getElementById('txtEmail').value,
    password: document.getElementById('txtPass').value,
    math: document.getElementById('txtDiemToan').value * 1,
    physics: document.getElementById('txtDiemLy').value * 1,
    chemistry: document.getElementById('txtDiemHoa').value * 1,
  };
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      // tatLoading();
      // console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      // tatLoading();
      console.log(err);
    })
    .finally(function () {
      tatLoading();
    })
}

function get1SV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      document.getElementById('txtMaSV').value = res.data.id;
      document.getElementById('txtTenSV').value = res.data.name;
      document.getElementById('txtEmail').value = res.data.email;
      document.getElementById('txtPass').value = res.data.password;
      document.getElementById('txtDiemToan').value = res.data.math;
      document.getElementById('txtDiemLy').value = res.data.physics;
      document.getElementById('txtDiemHoa').value = res.data.chemistry;
    })
    .catch(function (err) {
      console.log(err);
    })
    .finally(function () {
      tatLoading();
    })
}

function chuanbiSuaSV(id) {
  get1SV(id)
}

function capnhatSV(id) {
  let newInfo = {
    id: document.getElementById('txtMaSV').value,
    name: document.getElementById('txtTenSV').value,
    email: document.getElementById('txtEmail').value,
    password: document.getElementById('txtPass').value,
    math: document.getElementById('txtDiemToan').value * 1,
    physics: document.getElementById('txtDiemLy').value * 1,
    chemistry: document.getElementById('txtDiemHoa').value * 1,
  }

  if (newInfo.id.length > 0) {
    batLoading();
    axios({
      url: `${BASE_URL}/sv/${newInfo.id}`,
      method: "PUT",
      data: newInfo,
    })
      .then(function (res) {
        getDSSV();
        document.getElementById('txtMaSV').value = '';
        document.getElementById('txtTenSV').value = '';
        document.getElementById('txtEmail').value = '';
        document.getElementById('txtPass').value = '';
        document.getElementById('txtDiemToan').value = '';
        document.getElementById('txtDiemLy').value = '';
        document.getElementById('txtDiemHoa').value = '';
      })
      .catch(function (err) {
        console.log(err);
      })
      .finally(function () {
        tatLoading();
      })
  }


}

